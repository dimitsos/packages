# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=xorgproto
pkgver=2019.2
pkgrel=0
pkgdesc="X11 protocol headers (combination)"
url="https://www.X.Org/"
arch="noarch"
license="X11 AND MIT AND BSD-2-Clause"
depends=""
depends_dev="util-macros"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc"
# Taken from the .pc.in files.  Check these at every bump!
provides="bigreqsproto=1.1.2
	compositeproto=0.4.2
	damageproto=1.2.1
	dmxproto=2.3.1
	dri2proto=2.8
	dri3proto=1.2
	fixesproto=5.0
	fontsproto=2.1.3
	glproto=1.4.17
	inputproto=2.3.2
	kbproto=1.0.7
	presentproto=1.2
	printproto=1.0.5
	randrproto=1.6.0
	recordproto=1.14.2
	renderproto=0.11.1
	resourceproto=1.2.0
	scrnsaverproto=1.2.3
	trapproto=3.4.3
	videoproto=2.3.3
	xcmiscproto=1.2.2
	xextproto=7.3.0
	xf86bigfontproto=1.2.0
	xf86dgaproto=2.1
	xf86driproto=2.1.1
	xf86miscproto=0.9.3
	xf86vidmodeproto=2.3.1
	xineramaproto=1.2.1
	xproto=7.0.32
	xproxymngproto=1.0.3
"
replaces="bigreqsproto compositeproto damageproto dri2proto dri3proto
	fixesproto fontsproto glproto inputproto kbproto presentproto printproto
	randrproto recordproto renderproto resourceproto scrnsaverproto
	videoproto xcmiscproto xextproto xf86bigfontproto xf86dgaproto
	xf86driproto xf86miscproto xf86vidmodeproto xineramaproto xproto"
source="https://xorg.freedesktop.org/archive/individual/proto/xorgproto-$pkgver.tar.bz2
	xprint.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--datadir=/usr/lib \
		--enable-legacy
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	# libX11
	rm "$pkgdir"/usr/include/X11/extensions/XKBgeom.h
	# libXvMC
	rm "$pkgdir"/usr/include/X11/extensions/vldXvMC.h
}

sha512sums="cbfdf6bb3d58d4d4e7788c9ed779402352715e9899f65594fbc527b3178f1dc5e03cebc8ba5a863b3c196a1a0f2026c2d0438207ca19f81f3c8b7da0c0667904  xorgproto-2019.2.tar.bz2
18fc5228bb53eb15cfa0018f718e06faba0384a41e3aa0006fbf63e2a98779fdab527ea9eb8e22bb6a6f1ca02340ad8dad1260ee16e75f0416461ccefaa6df73  xprint.patch"
