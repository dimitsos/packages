# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi
pkgver=20.08.1
pkgrel=0
pkgdesc="Libraries and storage system for PIM data"
url="https://community.kde.org/KDE_PIM/Akonadi"
arch="all"
options="!check"  # Test suite requires running D-Bus session.
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="qt5-qtbase-postgresql"
depends_dev="qt5-qtbase-dev boost-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kio-dev kitemmodels-dev libxml2-dev"
makedepends="$depends_dev cmake extra-cmake-modules libxslt-dev qt5-qttools-dev
	libxslt-dev shared-mime-info sqlite-dev kcompletion-dev kcrash-dev
	kdesignerplugin-dev ki18n-dev kiconthemes-dev kitemviews-dev kio-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-$pkgver.tar.xz
	akonadiserverrc
	atomics.patch
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/config/akonadi
	install -m 644 "$srcdir"/akonadiserverrc \
		"$pkgdir"/usr/share/config/akonadi
}

sha512sums="e444f7449d4ac6ed184702d854785d3ce9a3c42878cb030934901a712a372cfc95c07d3e7b3ee70a1f0081a920f64d44372fbd194eb6c7c32b9938b69ab9ac5a  akonadi-20.08.1.tar.xz
b0c333508da8ba5c447827b2bad5f36e3dc72bef8303b1526043b09c75d3055790908ac9cbb871e61319cfd4b405f4662d62d2d347e563c9956f4c8159fca9ab  akonadiserverrc
6cdeece137523f0b67fac0384de446562ddca7530cf4a9cc7736d3eb582b6630e24a125f3648778f76b2b576b47e33d5b99a6bfd96770afa0dad97d1a1260cd1  atomics.patch
f9217e1632612206d0e86b4a3667544cf77218535109e2cdff4e9ddd84805bf8d04f38db3b84c909c405ac47c656e01fef170f1d48ebcd0c3e7bfadb3a0dd35c  lts.patch"
