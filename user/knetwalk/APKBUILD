# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knetwalk
pkgver=20.08.1
pkgrel=0
pkgdesc="Build up a computer network by placing the wires correctly"
url="https://www.kde.org/applications/games/knetwalk/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdoctools-dev
	kdbusaddons-dev ki18n-dev ktextwidgets-dev kwidgetsaddons-dev
	kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/knetwalk-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4258f52b501ee19c20ec08d3ad9f27e6c97a254fea83b4e87da90bb545c9fbf7d8a63687f0c3a7652bc93d84e9f2a96b45713d69dbfb1bce98e60d7c414d1477  knetwalk-20.08.1.tar.xz"
