# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=atril
pkgver=1.24.1
pkgrel=0
pkgdesc="Document viewer for the MATE desktop environment"
url="https://mate-desktop.org"
arch="all"
options="!check" # testsuite requires X and py3-dogtail
license="GPL-2.0+ AND Afmparse AND Info-ZIP AND libtiff AND LGPL-2.0+ AND MIT AND LGPL-2.1+"
depends=""
_regendepends="autoconf autoconf-archive-dev automake gtk-doc itstool mate-common
	mate-common-dev yelp-tools yelp-tools-dev"
makedepends="caja-dev djvulibre-dev gobject-introspection-dev gtk+3.0-dev
	intltool libgxps-dev libsecret-dev libsm-dev libspectre-dev
	libxml2-dev libxml2-utils poppler-dev python3 tiff-dev
	wayland-protocols $_regendepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://pub.mate-desktop.org/releases/${pkgver%.*}/atril-$pkgver.tar.xz
	fix-autogen.patch
	optional-synctex.patch
	"

# secfixes:
#   1.22.1-r1:
#     - CVE-2019-1010006
#   1.22.1-r2:
#     - CVE-2019-11459

build() {
	./autogen.sh \
		--disable-maintainer-mode \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-introspection \
		--enable-pixbuf \
		--enable-comics \
		--enable-xps \
		--disable-synctex
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="94a55e7699bdfc9368e20986664bd411c12f50f466874ebf20210df3de6ddb499866b505e157c56b58a8065aad2c24284b96afa8c547a6cd259eef413096a91a  atril-1.24.1.tar.xz
91d46a43267c5668c6831c81a1ee213564f833e64d8297c372b9ddc5037ae2d13594829a25b2a8086c46aa85068ad7a5024db4f5853ab009d5cbca9013fc9cc9  fix-autogen.patch
cb4493eca4e0bc1bd4a9bdd4540033aad3318023f6101f5548c14c7c7134ce6a4618849df44573af39af7d561ebca4b9b549668e81c50c7432a525db7d0f620b  optional-synctex.patch"
