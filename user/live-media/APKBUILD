# Maintainer: Zach van Rijn <me@zv.io>
pkgname=live-media
pkgver=2020.07.31
pkgrel=0
pkgdesc="Libraries for multimedia streaming"
url="http://live555.com/liveMedia"
arch="all"
options="!check"  # No test suite.
license="LGPL-3.0+"
depends=""
makedepends="openssl-dev"
subpackages="$pkgname-dev $pkgname-utils"
source="https://download.videolan.org/contrib/live555/live.$pkgver.tar.gz"
builddir="$srcdir"/live

prepare() {
	default_prepare
	sed -e "/^COMPILE_OPTS/s/$/ $CFLAGS -fPIC -DPIC -DRTSPCLIENT_SYNCHRONOUS_INTERFACE/" \
		-i config.linux-with-shared-libraries
}

build() {
	./genMakefiles linux-with-shared-libraries
	make C_COMPILER="${CC:-gcc}" CPLUSPLUS_COMPILER="${CXX:-g++}"
}

package() {
	mkdir -p "$pkgdir"/usr/lib
	for f in BasicUsageEnvironment UsageEnvironment liveMedia groupsock; do
		mkdir -p "$pkgdir"/usr/include/$f
		cp $f/include/*.h* "$pkgdir"/usr/include/$f
		for so in $f/lib*.so.*; do
			soname=$(scanelf -B --format "#F%S" $so)
			cp $so "$pkgdir"/usr/lib/
			so=${so##*/}
			ln -s ${so} "$pkgdir"/usr/lib/${soname}
			ln -s ${so} "$pkgdir"/usr/lib/${soname%.so.*}.so
		done
	done

	mkdir -p "$pkgdir"/usr/bin
	for testprog in `find testProgs -type f -perm 755`; do
		install ${testprog} "$pkgdir"/usr/bin
	done
}

utils() {
	pkgdesc="RTSP streaming tools"
	license="GPL-3.0+"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="e23f6da5b0b1767c959bc3591276f8e0678732d4f197b1369aea2963ef2bf1e37a9ac714deded783819c1d5b5f9319cff0ed15f349908d88986649dc16e2da15  live.2020.07.31.tar.gz"
