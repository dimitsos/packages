# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=libfm-qt
pkgver=0.17.1
_lxqt=0.9.0
pkgrel=0
pkgdesc="Qt library for file management and bindings for libfm"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
depends_dev="libfm-dev menu-cache-dev libexif-dev"
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt qt5-qttools-dev
	qt5-qtx11extras-dev $depends_dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/libfm-qt/releases/download/$pkgver/libfm-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -O0" \
		-DCMAKE_C_FLAGS="$CFLAGS -O0" \
		-DPULL_TRANSLATIONS=False \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="1e1781a01446f381625361a50f77422827e7ac5ab6039b4246369e2281edf78d3ccba866fa472667bcbb6737c44ebc350cb30a0055fc84d1f5d7e1c83e1241c1  libfm-qt-0.17.1.tar.xz"
