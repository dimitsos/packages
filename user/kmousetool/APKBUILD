# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmousetool
pkgver=20.08.1
pkgrel=0
pkgdesc="Tool to assist with clicking the mouse button"
url="https://userbase.kde.org/KMouseTool"
arch="all"
license="LGPL-2.1-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdbusaddons-dev ki18n-dev
	kdoctools-dev kiconthemes-dev knotifications-dev kxmlgui-dev phonon-dev
	libxtst-dev libxt-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmousetool-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4393e41e796705a0555be67a2fe6925593f72a4e738f70d3010d77bfaf65fe219edc8669a9c662397ad1d5d8d378e416c73ccc08fa15156c078a312e88cf74a3  kmousetool-20.08.1.tar.xz"
