# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kphotoalbum
pkgver=5.7.0
pkgrel=0
pkgdesc="Versatile photo album software by KDE"
url="https://www.kphotoalbum.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev phonon-dev karchive-dev
	kcompletion-dev kconfig-dev kcoreaddons-dev kdoctools-dev ki18n-dev
	kiconthemes-dev kjobwidgets-dev exiv2-dev ktextwidgets-dev kxmlgui-dev
	kwidgetsaddons-dev libjpeg-turbo-dev kio-dev libkipi-dev libkdcraw-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kphotoalbum/$pkgver/kphotoalbum-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="36153fd946c7819540b1867a24b968103f7101d1d9ddc49a09b70df4b419380875b3016863417ba8326b50fd844d255888cd5b6f2d4ecf1e1a607d0c97e16851  kphotoalbum-5.7.0.tar.xz"
