# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-pillow
_pkgname=Pillow
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=6.2.2
pkgrel=0
pkgdesc="A Python Imaging Library"
url="https://pypi.org/project/Pillow"
arch="all"
# Certified net clean
license="Custom"
depends="py3-olefile python3"
makedepends="python3-dev freetype-dev libjpeg-turbo-dev libwebp-dev
	tiff-dev libpng-dev lcms2-dev openjpeg-dev zlib-dev"
subpackages="$pkgname-doc"
_scripts_rev="b24479c"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/P/$_pkgname/$_pkgname-$pkgver.tar.gz
	https://dev.sick.bike/dist/$pkgname-scripts-$_scripts_rev.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# secfixes: pillow
#   6.2.2-r0:
#     - CVE-2019-19911
#     - CVE-2020-5310
#     - CVE-2020-5311
#     - CVE-2020-5312
#     - CVE-2020-5313

unpack() {
	default_unpack
	mv pillow-scripts-*/Scripts "$builddir/Scripts"
}

build() {
	# zlib resides in lib
	export CFLAGS="$CFLAGS -L/lib"
	python3 setup.py build
}

check() {
	PYTHONPATH="$(find 'build' -name 'lib.*')" python3 selftest.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

doc() {
	pkgdesc="Example scripts for $pkgname"
	depends="$pkgname"

	destdir="$subpkgdir/usr/share/doc/$pkgname"
	install -d "$destdir"/Scripts
	install -m 644 "$builddir"/Scripts/* "$destdir"/Scripts/
	install -Dm644 "$builddir"/LICENSE "$subpkgdir/usr/share/licenses/$pkgname/LICENSE"
}

sha512sums="517c971d4fb23a47466a7e8326c8c29291f7832d4521001c1898d6223ea25d4b0a8b7c8f7e78dd706f421229a8261b558b9fbdc43e47a0a2a7b2b4bbc1a21eff  py3-pillow-6.2.2.tar.gz
c01e83a7cef6653a33f60acbcbc737f0d40ff0dbc792ce0b2ce52f21092d3071845830fa0f64b27a1c5e679c53df57e0ec2e89867ee717f938d4e6f19db77790  py3-pillow-scripts-b24479c.tar.gz"
