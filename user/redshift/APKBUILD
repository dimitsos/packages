# Contributor: Mira Ressel <aranea@aixah.de>
# Maintainer: 
pkgname=redshift
pkgver=1.12
pkgrel=0
pkgdesc="Adjust the color temperature of your screen according to your surroundings"
url="http://jonls.dk/redshift/"
arch="all"
license="GPL-3.0+"
options="!check"  # No test suite.
depends=""
makedepends="glib-dev intltool libdrm-dev libx11-dev libxcb-dev libxxf86vm-dev"
subpackages="$pkgname-doc $pkgname-gui $pkgname-lang"
source="https://github.com/jonls/redshift/releases/download/v$pkgver/redshift-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-rpath \
		--disable-corelocation \
		--enable-drm \
		--disable-geoclue2 \
		--enable-nls \
		--disable-quartz \
		--enable-randr \
		--enable-vidmode \
		--disable-wingdi
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

doc() {
	install -Dm644 -t "$subpkgdir/usr/share/doc/$pkgname" \
		"$builddir"/redshift.conf.sample
	default_doc
}

gui() {
	depends="py3-pygobject"

	install -d "$subpkgdir/usr/bin" "$subpkgdir/usr/lib" "$subpkgdir/usr/share"
	mv "$pkgdir/usr/bin/redshift-gtk" "$subpkgdir/usr/bin/"
	mv "$pkgdir/usr/lib/python"* "$subpkgdir/usr/lib/"
	mv "$pkgdir/usr/share/appdata" "$pkgdir/usr/share/applications" "$pkgdir/usr/share/icons" "$subpkgdir/usr/share/"
}

sha512sums="225e222e5f2c55be4571094ccaf02a92e162dfc35fd0fe504084e21e358b888a72f9992f9f9edaf1d496eb673af74a0d825ae5cf6ef7f0f1ab51d32419722c32  redshift-1.12.tar.xz"
