# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Carlo Landmeter
# Maintainer: 
pkgname=wavpack
pkgver=5.3.0
pkgrel=0
pkgdesc="Audio compression format with lossless, lossy, and hybrid compression modes"
url="http://www.wavpack.com/"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause"
depends=""
makedepends=""
subpackages="$pkgname-dev $pkgname-doc"
source="http://www.wavpack.com/$pkgname-$pkgver.tar.bz2"

# secfixes:
#   5.2.0-r0:
#     - CVE-2018-6767
#     - CVE-2018-7253
#     - CVE-2018-7254
#     - CVE-2018-10536
#     - CVE-2018-10537
#     - CVE-2018-10538
#     - CVE-2018-10539
#     - CVE-2018-10540
#     - CVE-2019-11498
#     - CVE-2019-1010315
#     - CVE-2019-1010317
#     - CVE-2019-1010319
#   5.1.0-r4:
#     - CVE-2018-19840
#     - CVE-2018-19841
#   5.1.0-r0:
#     - CVE-2016-10169
#     - CVE-2016-10170
#     - CVE-2016-10171
#     - CVE-2016-10172

build() {
	_arch_opts=""
	case "$CARCH" in
	x86 | x86_64) _arch_opts="--enable-mmx" ;;
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-static \
		$_arch_opts
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2c5038abfbe7ab3b51fb30b3fc8b636117e9afe1821c40832fbdfb960d1153e0cb4a68dd84a89b205c3cdc10030d3aa7a7340b296d9c148e8847471c2e7c0cd1  wavpack-5.3.0.tar.bz2"
