# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=claws-mail
pkgver=3.17.8
pkgrel=0
pkgdesc="User-friendly, lightweight, and fast email client"
url="https://www.claws-mail.org/"
arch="all"
license="GPL-3.0-only"
depends="compface"
makedepends="compface-dev curl-dev dbus-glib-dev enchant-dev gnutls-dev
	gpgme-dev gtk+2.0-dev libcanberra-gtk2 libcanberra-dev libetpan-dev
	libical-dev libnotify-dev librsvg-dev openldap-dev
	startup-notification-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://www.claws-mail.org/download.php?file=releases/claws-mail-$pkgver.tar.xz"

# secfixes:
#   3.17.8-r0:
#     - CVE-2020-16094

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-perl-plugin \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="dc29c968dc81a184af8f66c1afe5c9d17558ce6a4a8b196136a9fb5deec96aa67eec42148ed0f4d6d6ee94aec2791247b9034090dac81beec193bd7d366617d7  claws-mail-3.17.8.tar.xz"
