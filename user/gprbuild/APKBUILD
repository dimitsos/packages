# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=gprbuild
pkgver=2020
_pkgver=2020-20200814-19ABE
_xmlver=2020-20200814-19BC4
pkgrel=0
pkgdesc="An advanced build system for multi-language systems"
url="https://github.com/AdaCore/gprbuild"
arch="all"
options="!check" # No test suite.
license="GPL-3.0+"
depends=""
makedepends="gcc-gnat"
source="$pkgname-$_pkgver-src.tar.gz::https://community.download.adacore.com/v1/4e13d41920eac86fd139b5d7984eb908d697d868?filename=$pkgname-$_pkgver-src.tar.gz
	xmlada-$_xmlver-src.tar.gz::https://community.download.adacore.com/v1/9cf1ab59b526d1260e007fa9719126c5498632d2?filename=xmlada-$_xmlver-src.tar.gz
	foxkit.xml
	"
builddir="$srcdir/$pkgname-$_pkgver-src"

build() {
	xmlada="../xmlada-$_xmlver-src"
	incflags="-Isrc -Igpr/src -I$xmlada/dom -I$xmlada/input_sources \
	          -I$xmlada/sax -I$xmlada/schema -I$xmlada/unicode"
	gcc -c ${CFLAGS} gpr/src/gpr_imports.c -o gpr_imports.o
	for bin in gprbuild gprconfig gprclean gprinstall gprls gprname; do
		gnatmake -j$JOBS $incflags $ADAFLAGS $bin-main \
			-o $bin -cargs $CFLAGS -largs $LDFLAGS gpr_imports.o
	done
	for lib in gprlib gprbind; do
		gnatmake -j$JOBS $incflags $ADAFLAGS $lib \
			-cargs $CFLAGS -largs $LDFLAGS gpr_imports.o
	done
}

package() {
	mkdir -p "$pkgdir"/usr/bin
	cp gprbuild gprconfig gprclean gprinstall gprls gprname \
		"$pkgdir"/usr/bin
	mkdir -p "$pkgdir"/usr/libexec/gprbuild
	cp gprlib gprbind \
		"$pkgdir"/usr/libexec/gprbuild
	mkdir -p "$pkgdir"/usr/share/gpr
	cp share/_default.gpr \
		"$pkgdir"/usr/share/gpr
	mkdir -p "$pkgdir"/usr/share/gprconfig
	cp share/gprconfig/* "$srcdir"/foxkit.xml \
		"$pkgdir"/usr/share/gprconfig
}

sha512sums="cd9cb2a1f7867f17a5b69d8500476f3a97ca4856b171344d3bf887f55a4860d0ae32ec89ad82e9c7cec13fa20e58cd8ab890554115774095111ed38f9760050e  gprbuild-2020-20200814-19ABE-src.tar.gz
51365bc1baaa0886caf46d75d3d27d56b37d5a02f04673321a60bda14a2adda0edd43321f8b34583d4aeaf852f4a21a924f841899c458076b3f261d881bb29a1  xmlada-2020-20200814-19BC4-src.tar.gz
e369c094963d3dcfb03d7ac0949825531bae6410ef9c4bec774cb0da70d2bd4a784bdec37db5151c0371ce769712ee02fc04f36896ccc8bddcdb585c1ee8dbbc  foxkit.xml"
