###################################
# Default lighttpd.conf for Adélie
###################################


######## Variables ########
var.basedir  = "/var/www/localhost"
var.logdir   = "/var/log/lighttpd"
var.statedir = "/var/lib/lighttpd"


######## Modules ########
# NOTE: the order of modules is important.
server.modules = (
#	"mod_rewrite",
#	"mod_redirect",
#	"mod_alias",
	"mod_access",
#	"mod_cml",
#	"mod_trigger_b4_dl",
#	"mod_auth",
#	"mod_status",
	"mod_setenv",
#	"mod_proxy",
#	"mod_simple_vhost",
#	"mod_evhost",
#	"mod_userdir",
	"mod_compress",
#	"mod_ssi",
#	"mod_usertrack",
	"mod_expire",
#	"mod_secdownload",
#	"mod_rrdtool",
#	"mod_webdav",
	"mod_accesslog",
#       "mod_openssl"   # You need this if you want TLS!
)


######## Inclusions ########
include "mime-types.conf"
# uncomment for cgi support
#   include "mod_cgi.conf"
# uncomment for php/fastcgi support
#   include "mod_fastcgi.conf"
# uncomment for php/fastcgi fpm support
#   include "mod_fastcgi_fpm.conf"


######## Global Settings ########
server.username      = "lighttpd"
server.groupname     = "lighttpd"

server.document-root = var.basedir + "/htdocs"
server.pid-file      = "/run/lighttpd.pid"

server.errorlog      = var.logdir  + "/error.log"
# To log errors to syslog instead, use:
# server.errorlog-use-syslog = "enable"

server.indexfiles    = ("index.html", "index.htm", "default.htm")

# server.tag           = "lighttpd"

server.follow-symlink = "enable"

server.event-handler = "linux-sysepoll"

# To chroot to a directory:
# server.chroot      = "/"

# Default bind port is 80.  To change:
# server.port          = 81

# Default bind address is global (0.0.0.0).  To change:
# server.bind          = "grisu.home.kneschke.de"

# error-handler for status 404
# server.error-handler-404 = "/error-handler.html"

# Format: <errorfile-prefix><status-code>.html
# -> ..../status-404.html for 'File not found'
# server.errorfile-prefix    = var.basedir + "/error/status-"

# FAM support for caching stat() calls
server.stat-cache-engine = "fam"

# which extensions should not be handled via static-file transfer
# (extensions that are usually handled by mod_cgi, mod_fastcgi, etc).
static-file.exclude-extensions = (".php", ".pl", ".cgi", ".fcgi")


######## mod_accesslog config ########
accesslog.filename   = var.logdir + "/access.log"


######## mod_dirlisting config ########
# Enable directory listings if no indexfile is present.
#dir-listing.activate      = "enable"

# Don't list hidden files/directories (beginning with '.')
#dir-listing.hide-dotfiles = "enable"
#
# Specify a path here for custom directory listing CSS:
#dir-listing.external-css  = "/path/to/dir-listing.css"
#
# Exclude files that match any regex in this list from directory listings:
#dir-listing.exclude = ("^\.", "~$")


######## mod_access config ########
# See access.txt in lighttpd-doc package for more info.

url.access-deny = ("~", ".inc")


######## mod_userdir config ########
# This will give all users with valid homedirs the chance to publish a
# webpage from this server using traditional /~username/ paths.
# See userdir.txt in lighttpd-doc package for more info.
#
#userdir.path = "public_html"
#userdir.exclude-user = ("root")


######## mod_ssi config ########
# This allows you to use server-side includes.
#ssi.extension = (".shtml")


######## SSL config ########
# See ssl.txt in lighttpd-doc package for more info.
# The defaults here are NOT the server defaults.  You need to uncomment
# them to use them.  They are HIGHLY recommended; by default, lighttpd
# will serve older TLS protocols that may be vulnerable to attack.
#
#ssl.engine				= "enable"
#ssl.honor-cipher-order			= "enable"
#ssl.disable-client-renegotiation	= "enable"
# pemfile is cert+privkey, ca-file is the intermediate chain in one file
#ssl.pemfile				= "/path/to/signed_cert_plus_private_key.pem"
#ssl.ca-file				= "/path/to/intermediate_certificate.pem"
# ECDH/ECDHE ciphers curve strength (see `openssl ecparam -list_curves`)
#ssl.ec-curve				= "secp384r1"
# Environment flag for HTTPS enabled
#setenv.add-environment = (
#	"HTTPS" => "on"
#)
# Modern configuration, tweak to your needs
#ssl.use-sslv2				= "disable"
#ssl.use-sslv3				= "disable"
#ssl.cipher-list			= "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256"
# HSTS(15768000 seconds = 6 months)
#setenv.add-response-header  = (
#	"Strict-Transport-Security" => "max-age=15768000;"
#)


######## mod_status config ########
# This is generally handy and won't expose any sensitive info.
#status.status-url  = "/server-status"
# This will expose some of your configuration to the world!
#status.config-url  = "/server-config"


######## mod_compress config ########
compress.cache-dir	= var.statedir + "/cache/compress"
compress.filetype	= ("text/plain", "text/html")


######## mod_proxy config ########
# See proxy.txt in lighttpd-doc package for more info.
# proxy.server               = ( ".php" =>
#                               ( "localhost" =>
#                                 (
#                                   "host" => "192.168.0.101",
#                                   "port" => 80
#                                 )
#                               )
#                             )
# }}}


######## mod_auth config ########
# See authentication.txt in lighttpd-doc package for more info.
#auth.backend               = "plain"
#auth.backend.plain.userfile = "lighttpd.user"
#auth.backend.plain.groupfile = "lighttpd.group"

#auth.backend.ldap.hostname = "localhost"
#auth.backend.ldap.base-dn  = "dc=my-domain,dc=com"
#auth.backend.ldap.filter   = "(uid=$)"

#auth.require               = ( "/server-status" =>
#                               (
#                                 "method"  => "digest",
#                                 "realm"   => "download archiv",
#                                 "require" => "user=jan"
#                               ),
#                               "/server-info" =>
#                               (
#                                 "method"  => "digest",
#                                 "realm"   => "download archiv",
#                                 "require" => "valid-user"
#                               )
#                             )


######## mod_rewrite config ########
# Apache-style mod_rewrite for implementing URL rewriting.
# See rewrite.txt in lighttpd-doc package for more info.
#
#url.rewrite = (
#	"^/$"		=>		"/server-status"
#)


######## mod_redirect config ########
# See redirect.txt in lighttpd-doc package for more info.
#
#url.redirect = (
#	"^/wishlist/(.+)"		=>		"http://www.123.org/$1"
#)


######## mod_expire config ########
# It is highly recommended you configure Expire: headers correctly to
# conserve bandwidth, especially for users on slow links.
#expire.url = (
#	"/buggy/"		=>		"access 2 hours",
#	"/asdhas/"		=>		"access plus 1 seconds 2 minutes"
#)

# {{{ mod_trigger_b4_dl
# see trigger_b4_dl.txt
#
# trigger-before-download.gdbm-filename = "/home/weigon/testbase/trigger.db"
# trigger-before-download.memcache-hosts = ( "127.0.0.1:11211" )
# trigger-before-download.trigger-url = "^/trigger/"
# trigger-before-download.download-url = "^/download/"
# trigger-before-download.deny-url = "http://127.0.0.1/index.html"
# trigger-before-download.trigger-timeout = 10
# }}}


######## mod_webdav config ########
# lighttpd can act as a WebDAV server.
# See webdav.txt in lighttpd-doc package for more info.
#
#$HTTP["url"] =~ "^/dav($|/)" {
#	webdav.activate = "enable"
#	webdav.is-readonly = "enable"
#}


######## Debugging options ########
# debug.log-request-header   = "enable"
# debug.log-response-header  = "enable"
# debug.log-request-handling = "enable"
# debug.log-file-not-found   = "enable"

# vim: set ft=conf foldmethod=marker et :
