# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cantor
pkgver=20.08.1
pkgrel=0
pkgdesc="KDE worksheet interface for popular mathematical applications"
url="https://edu.kde.org/cantor/"
arch="all"
options="!check"  # Tests require X11.
license="GPL-2.0-only"
depends="r"
depends_dev="libspectre-dev"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev karchive-dev kcompletion-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kio-dev knewstuff-dev kparts-dev kpty-dev ktexteditor-dev
	ktextwidgets-dev kxmlgui-dev $depends_dev poppler-dev poppler-qt5-dev
	
	analitza-dev gfortran libqalculate-dev python3-dev r r-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/cantor-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7f3193753a3368940cad14b51cb920f59a2c5550ef2ae527869c1011b238533e15b0d89b47c29b383d623709e7576457d9bc803a63124c641e58871115e72d6c  cantor-20.08.1.tar.xz"
