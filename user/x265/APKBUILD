# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=x265
pkgver=3.2.1
pkgrel=0
pkgdesc="Open Source H265/HEVC video encoder"
url="http://x265.org"
arch="all"
options="textrels"
license="GPL-2.0-only"
depends=""
makedepends="cmake yasm" # 2.6+ will use nasm instead of yasm
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://download.videolan.org/pub/videolan/${pkgname}/${pkgname}_${pkgver}.tar.gz
	x265-unbreak-my-arm.patch"
builddir="$srcdir/${pkgname}_$pkgver"

build() {
	cd "$builddir"/build/linux

	cmake_opts=""
	case "$CARCH" in
		# unfortunately, AltiVec code is very broken (more than 1000 errors)
		ppc*) cmake_opts="-DENABLE_ALTIVEC=OFF -DCPU_POWER8=OFF";;
	esac

	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		$cmake_opts \
		../../source
	make
}

check() {
	cd "$builddir"/build/linux

	# LEAVE THIS.
	# every breakage so far has been SIGILL, which IS detected by this.
	# LEAVE IT!
	./x265 --version
}

package() {
	cd "$builddir"/build/linux

	make DESTDIR="$pkgdir" install
}

sha512sums="5cb29b9d4475c1f686f6e31e47ccddde3db1639d5dcf9c058513389a9fdeb35a83b14e30e59498a76dec74bb0b410b6d8f5b1b45d18927be9811bb6a40d8d568  x265-3.2.1.tar.gz
ac3ecc613fe4da11672de8f397b5541fe2af29b7dd91a48859bfe8824934a5966c41ca30de242564c46f1a0cdd2a0da9331d9f547fc21120066fb3c45347c4c4  x265-unbreak-my-arm.patch"
