# Maintainer: 
pkgname=gtest
pkgver=1.10.0
pkgrel=0
pkgdesc="C++ testing framework based on xUnit (like JUnit)"
url="https://github.com/google/googletest"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause"
depends="libgcc bash"
depends_dev="python3 cmake"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/google/googletest/archive/release-$pkgver.tar.gz"
builddir="$srcdir"/googletest-release-${pkgver}

build() {
	rm -rf build
	mkdir build
	cd build

	cmake -DBUILD_SHARED_LIBS=ON \
	      -DCMAKE_SKIP_RPATH=ON ../
	make
}

package() {
	for dir in usr/lib usr/include/gtest/internal/custom usr/share/licenses/gtest\
			usr/src/gtest/cmake usr/src/gtest/src; do
		install -d -m 0755 "$pkgdir"/"$dir"
	done
	install -m 0644 build/lib/libgtest*.so "$pkgdir"/usr/lib

	install -m 0644 googletest/include/gtest/*.h "$pkgdir"/usr/include/gtest
	install -m 0644 googletest/include/gtest/internal/*.h \
		"$pkgdir"/usr/include/gtest/internal/
	install -m 0644 googletest/include/gtest/internal/custom/*.h \
		"$pkgdir"/usr/include/gtest/internal/custom/
	install -m 0644 googletest/LICENSE "$pkgdir"/usr/share/licenses/$pkgname/
	install -m 0644 googletest/CMakeLists.txt "$pkgdir"/usr/src/gtest/
	install -m 0644 googletest/cmake/* "$pkgdir"/usr/src/gtest/cmake/
}

sha512sums="bd52abe938c3722adc2347afad52ea3a17ecc76730d8d16b065e165bc7477d762bce0997a427131866a89f1001e3f3315198204ffa5d643a9355f1f4d0d7b1a9  release-1.10.0.tar.gz"
