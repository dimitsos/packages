# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: 
pkgname=rrdtool
pkgver=1.7.2
pkgrel=0
pkgdesc="Data logging and graphing application"
url="https://oss.oetiker.ch/rrdtool/"
arch="all"
license="GPL-2.0+"
depends="font-sony-misc"
checkdepends="bc"
makedepends="cairo-dev freetype-dev groff libart-lgpl-dev libpng-dev
	libxml2-dev pango-dev perl-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang perl-rrd:perl_rrd
	$pkgname-cached $pkgname-cgi $pkgname-utils librrd:libs"
source="https://oss.oetiker.ch/$pkgname/pub/$pkgname-$pkgver.tar.gz
	disable-rpn2.patch
	rrdcached.initd
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-tcl \
		--disable-ruby \
		--enable-rrdcgi \
		--enable-perl-site-install \
		--with-perl-options="INSTALLDIRS=vendor"
	make
}

check() {
	LANG=C.UTF-8 LC_ALL=C.UTF-8 TZ=UTC make check
}

package() {
	export INSTALLDIRS=vendor
	make DESTDIR="$pkgdir" install
	find "$pkgdir" -name '.packlist' -delete
	find "$pkgdir" -name 'perllocal.pod' -delete
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$pkgdir"/usr/share/$pkgname/examples "$subpkgdir"/usr/share/$pkgname/
}

perl_rrd() {
	depends=""
	pkgdesc="Perl interface for rrdtool"
	mkdir -p "$subpkgdir"/usr/lib \
		"$pkgdir"/usr/share
	mv "$pkgdir"/usr/lib/perl* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/share/perl* "$subpkgdir"/usr/share/
}

cached() {
	depends=""
	pkgdesc="RRDtool data caching daemon"
	mkdir -p "$subpkgdir"/usr/sbin \
		"$subpkgdir"/var/lib/rrdcached/db \
		"$subpkgdir"/var/lib/rrdcached/journal
	mv "$pkgdir"/usr/bin/rrdcached "$subpkgdir"/usr/sbin
	install -Dm755 "$srcdir"/rrdcached.initd "$subpkgdir"/etc/init.d/rrdcached
}

cgi() {
	depends=""
	pkgdesc="Create web pages containing RRD graphs based on templates"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/rrdcgi "$subpkgdir"/usr/bin
}

utils() {
	depends=""
	pkgdesc="RRDtool standalone utilities"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/rrdinfo \
		"$pkgdir"/usr/bin/rrdcreate \
		"$pkgdir"/usr/bin/rrdupdate \
		"$subpkgdir"/usr/bin
}

sha512sums="453230efc68aeb4a12842d20a9d246ba478a79c2f6bfd9693a91837c1c1136abe8af177be64fe29aa40bf84ccfce7f2f15296aefe095e89b8b62aef5a7623e29  rrdtool-1.7.2.tar.gz
47fbfa1ee974d607c15d7a49b1007ccfb6dcd2764f82236f8741b96fe1c3cdec8535f570116a2b24767e09c018177bd5ace4f2ceb39342b2baf3baec01a35dc1  disable-rpn2.patch
c0c27b2c2dfa8e7ec1cb1160d2bda8d7996bbea67f4ce7779da029f583c35e5e415cf46e2a1e5cb8ed2e63d2c58a68fd7471ee6bd820db4c0f4eeeb5c252f8a3  rrdcached.initd"
