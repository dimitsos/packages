# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ktorrent
pkgver=5.2.0
pkgrel=0
pkgdesc="BitTorrent client by KDE"
url="https://www.kde.org/applications/internet/ktorrent/"
arch="all"
options="!check"  # Test requires X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtscript-dev kio-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev ki18n-dev
	kdbusaddons-dev kiconthemes-dev knotifications-dev knotifyconfig-dev
	kcmutils-dev kparts-dev kservice-dev solid-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev libktorrent-dev taglib-dev kplotting-dev
	kdnssd-dev plasma-workspace-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/ktorrent/$pkgver/ktorrent-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="141976612cfe6bf4bd06e96842d7bc51ced7a3ab8e13c25446a00c031054698f851fbb1a31f3425dda5a428408bb1ceb51a4a420eaf2e79193b17a0cc7d7a387  ktorrent-5.2.0.tar.xz"
