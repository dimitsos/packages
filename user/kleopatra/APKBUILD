# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kleopatra
pkgver=20.08.1
pkgrel=0
pkgdesc="Certificate manager and cryptography GUI"
url="https://www.kde.org/applications/utilities/kleopatra/"
arch="all"
options="!check"  # Test requires D-Bus session bus.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libkleo-dev kcmutils-dev
	libassuan-dev kitemmodels-dev kmime-dev knotifications-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kleopatra-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6cbd3caa8c773380e4b7f7b396ba45f7a21ae39afc3e6b55d507ea0ef53c6a5663635de1ff494c325b88a57918665485a68c8100b67fe77292b632451b67e63c  kleopatra-20.08.1.tar.xz
18cba4b5562abcaaa6e949b46e9bdcce3f5af9f56233e397e1cd5cef8d5773775f077a4e332694cabf33dba423cd82a9d7e4627a4dc1a39e75bb17188f764f00  lts.patch"
