# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=qtermwidget
pkgver=0.17.0
_lxqt=0.9.0
pkgrel=0
pkgdesc="Qt-based terminal widget, used in QTerminal"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
depends_dev="libutempter-dev"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev
	lxqt-build-tools>=$_lxqt $depends_dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/qtermwidget/releases/download/$pkgver/qtermwidget-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DQTERMWIDGET_USE_UTEMPTER=True \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
        CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="c8f75dcd5182a6eea933452c89126fbe64035b278e8e9817d6966693f741691244e5766c49046e83add8f38cb00ff08e2640d2fa149c5aa46b20d8a06a22e78f  qtermwidget-0.17.0.tar.xz"
