# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=bluedevil
pkgver=5.18.5
pkgrel=0
pkgdesc="Bluetooth framework for KDE"
url="https://userbase.kde.org/Bluedevil"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND GPL-3.0+"
depends="kded shared-mime-info"
makedepends="bluez-qt-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev
	kded-dev kiconthemes-dev kio-dev kitemviews-dev kjobwidgets-dev
	knotifications-dev kpackage-dev kservice-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev plasma-framework-dev qt5-qtbase-dev
	qt5-qtdeclarative-dev solid-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/bluedevil-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4e5e55626d02a7ae820a43e760397e3b221749cd940569d180a67adba141f9afce418f1cb07c9fb1e607757461317a76343b5c6b86b3940a96711be9b4b66323  bluedevil-5.18.5.tar.xz"
