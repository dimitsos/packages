# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=ntfs-3g
_pkgname=ntfs-3g_ntfsprogs
pkgver=2017.3.23
pkgrel=2
pkgdesc="Stable, full-featured, read-write NTFS"
url="https://www.tuxera.com/community/open-source-ntfs-3g/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1-only AND BSD-2-Clause AND GPL-2.0+ AND GPL-3.0+"
depends=""
makedepends="attr-dev util-linux-dev linux-headers fuse-dev"
subpackages="$pkgname-doc $pkgname-dev $pkgname-libs"
source="https://tuxera.com/opensource/$_pkgname-$pkgver.tgz
	CVE-2019-9755.patch
	"
builddir="$srcdir/$_pkgname-$pkgver"

# secfixes:
#   2017.3.23-r2:
#     - CVE-2019-9755

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-fuse=external
	make
}

package() {
	mkdir -p "$pkgdir"/lib
	make -j1 DESTDIR="$pkgdir" LDCONFIG=: install
	ln -s /bin/ntfs-3g "$pkgdir"/sbin/mount.ntfs
}

sha512sums="3a607f0d7be35204c992d8931de0404fbc52032c13b4240d2c5e6f285c318a28eb2a385d7cf5ac4cd445876aee5baa5753bb636ada0d870d84a9d3fdbce794ef  ntfs-3g_ntfsprogs-2017.3.23.tgz
c79ae27e3c9490f0f893a16f27bb19c2cef2fe7b098aabca392163f4105b7ee9797b648d1013ce4c096adf639f6da2b8c43829cfabcc6ac3208c07454a6c0c5c  CVE-2019-9755.patch"
