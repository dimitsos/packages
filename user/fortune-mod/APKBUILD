# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=fortune-mod
pkgver=2.12.0
pkgrel=0
pkgdesc="Display random messages or quotations"
url="https://www.shlomifish.org/humour/fortunes/"
arch="all"
options="!check"  # Fails due to spaces in CMake files...
license="BSD-4-Clause"
depends=""
checkdepends="perl-file-find-object perl-io-all perl-test-differences
	perl-test-runvalgrind valgrind"
makedepends="cmake recode-dev rinutils"
subpackages="$pkgname-doc"
source="https://github.com/shlomif/fortune-mod/archive/$pkgname-$pkgver.tar.gz"
builddir="$srcdir/fortune-mod-fortune-mod-$pkgver/$pkgname"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DNO_OFFENSIVE=True \
		-DLOCALDIR=/usr/share/games/fortunes \
		${CMAKE_CROSSOPTS} \
		-Bbuild \
		.
	make -C build
}

check() {
	make -C build check
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="e3feca02b33bcae3c7113432af5d200d7c8701338c1d778c5dd893e4541969146185393c952f19f1b2f17077c4f171cdacbd9b6ba3cb0581117b8aa569cc652f  fortune-mod-2.12.0.tar.gz"
