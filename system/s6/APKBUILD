# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6
pkgver=2.11.0.0
pkgrel=0
pkgdesc="skarnet.org's small & secure supervision software suite"
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.11
depends="execline"
makedepends="skalibs-dev>=$_skalibs_version execline-dev"
install="$pkgname.post-upgrade"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
triggers="$pkgname.trigger=/run/service"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--enable-allstatic \
		--enable-static-libc \
		--libdir=/usr/lib \
		--libexecdir="/lib/$pkgname" \
		--with-dynlib=/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install
	mkdir -p -m 0755 "$pkgdir/var/lib/s6/services"
}


libs() {
        pkgdesc="$pkgdesc (shared libraries)"
        depends="skalibs-libs>=$_skalibs_version"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
        pkgdesc="$pkgdesc (development files)"
        depends="skalibs-dev>=$_skalibs_version"
        install_if="dev $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr"
        mv "$pkgdir/usr/lib" "$pkgdir/usr/include" "$subpkgdir/usr/"
}


libsdev() {
        pkgdesc="$pkgdesc (development files for dynamic linking)"
        depends="$pkgname-dev"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=""
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="5421b01de6d4f75e80056d3335cef409b3eb1330f1585c8b303228a36e60537056bed130f3f375f3f02f9063be04fc8776c4687cb0ec23e969960ad3fb5141e3  s6-2.11.0.0.tar.gz"
