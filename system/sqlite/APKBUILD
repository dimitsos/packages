# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sqlite
pkgver=3.31.1
_pkgver=${pkgver%.*}0${pkgver#*.*.}00
_pkgver=${_pkgver%.*}${_pkgver#*.}
pkgrel=0
pkgdesc="C library that implements an SQL database engine"
url="https://sqlite.org/"
arch="all"
options="!check"  # Test suite requires tcl - circular dependency
license="Public-Domain"
depends=""
makedepends="libedit-dev zlib-dev"
source="https://sqlite.org/2020/$pkgname-autoconf-$_pkgver.tar.gz"
subpackages="$pkgname-doc $pkgname-dev $pkgname-libs"
builddir="$srcdir/$pkgname-autoconf-$_pkgver"

# secfixes:
#   3.31.1-r0:
#     - CVE-2019-19242
#     - CVE-2019-19244
#     - CVE-2019-19317
#     - CVE-2019-19603
#     - CVE-2019-19645
#     - CVE-2019-19646
#     - CVE-2019-19880
#     - CVE-2019-19923
#     - CVE-2019-19924
#     - CVE-2019-19925
#     - CVE-2019-19926
#     - CVE-2019-19959
#     - CVE-2019-20218

build() {
	local _amalgamation="-DSQLITE_ENABLE_FTS4 \
	-DSQLITE_ENABLE_FTS3 \
	-DSQLITE_ENABLE_FTS3_PARENTHESIS \
	-DSQLITE_ENABLE_FTS5 \
	-DSQLITE_ENABLE_COLUMN_METADATA \
	-DSQLITE_SECURE_DELETE \
	-DSQLITE_ENABLE_UNLOCK_NOTIFY \
	-DSQLITE_ENABLE_RTREE \
	-DSQLITE_USE_URI \
	-DSQLITE_ENABLE_DBSTAT_VTAB \
	-DSQLITE_MAX_VARIABLE_NUMBER=250000 \
	-DSQLITE_ENABLE_JSON1"
	export CFLAGS="$CFLAGS $_amalgamation"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-threadsafe \
		--enable-static \
		--enable-editline \
		--enable-dynamic-extensions

	# rpath removal
	sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
	sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	install -Dm0644 sqlite3.1 \
		"$pkgdir"/usr/share/man/man1/sqlite3.1
}

libs() {
	pkgdesc="SQLite embeddable library"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr/
}

sha512sums="d85250ba5f78f0c918880f663741709aba662192f04888b2324a0df17affdf5b8540e8428c6c7315119806e7adad758ea281c9b403c0ad94ac6a9bd1b93fd617  sqlite-autoconf-3310100.tar.gz"
